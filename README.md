# th14bonus

東方輝針城の0.5ボーナスの取得回数を表示するツールです。

[![Build status](https://ci.appveyor.com/api/projects/status/qkp9wa9wla05p5vq/branch/master?svg=true)](https://ci.appveyor.com/project/ide-an/th14bonus/branch/master)

[Download](https://bitbucket.org/ide_an/th14bonus/downloads/)

## 対象

東方輝針城製品版 ver 1.00b

## 使用方法

 1. 輝針城を起動する
 2. th14bonus.exeを起動する
 3. なんかテキストボックスにボーナス取得回数が出る

## 免責事項

本プログラムの実行は自己責任によって行ってください。
念のためscore.datなどのバックアップを推奨します。
不具合の問い合わせについては@ide_anにお願いします。

## 更新履歴

20170708 v1 公開
