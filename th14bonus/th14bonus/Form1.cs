﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace th14bonus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void form1_Load(object sender, EventArgs e)
        {
            StartMonitor();
        }

        private bool is_monitor_alive;
        //監視スレッドの開始
        private void StartMonitor()
        {
            Thread monitor_thread = new Thread(new ThreadStart(TH11MonitorThread));
            is_monitor_alive = true;
            monitor_thread.Start();
            ui_timer.Start();
        }
        //監視スレッドの終了
        private void EndMonitor()
        {
            is_monitor_alive = false;
            ui_timer.Stop();
        }

        volatile int bonus_count = -1;

        //ゲーム監視スレッド
        private void TH11MonitorThread()
        {
            Th14Monitor th14 = new Th14Monitor();
            if (!th14.Init())
            {
                is_monitor_alive = false;
                return;
            }
            while (true)
            {
                if (!is_monitor_alive)
                {
                    break;
                }
                int tmp_bonus;
                bool success = th14.Update(out tmp_bonus);
                if (!success)
                {
                    break;
                }
                bonus_count = tmp_bonus;
                System.Threading.Thread.Sleep(16);
            }
            th14.Dispose();
            th14 = null;
            is_monitor_alive = false;
            GC.Collect();//あとしまつ
        }

        private void Tick(object sender, EventArgs e)
        {
            bonusCountTextBox.Text = bonus_count.ToString();
        }
    }
}
