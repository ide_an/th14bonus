﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace th14bonus
{
    class Win32
    {

        [DllImport("kernel32.dll")]
        public extern static IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            PROCESS_VM_READ = 0x10,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        [DllImport("kernel32.dll")]
        public extern static bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll")]
        public extern static bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out uint vNumberOfBytesRead);

        public enum ShoWindowFlags : int
        {
            HIDE = 0,
            SHOWNORMAL = 1,
            NORMAL = 1,
            SHOWMINIMIZED = 2,
            SHOWMAXIMIZED = 3,
            MAXIMIZE = 3,
            SHOWNOACTIVATE = 4,
            SHOW = 5,
            MINIMIZE = 6,
            SHOWMINNOACTIVE = 7,
            SHOWNA = 8,
            RESTORE = 9,
            SHOWDEFAULT = 10,
            FORCEMINIMIZE = 11
        }

        [DllImport("User32.dll")]
        public extern static bool ShowWindow(IntPtr hWnd, ShoWindowFlags nCmdShow);

        [DllImport("User32.dll")]
        public extern static bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("User32.dll")]
        public extern static IntPtr GetForegroundWindow();
        //指定したプロセスから指定したサイズのメモリを読み込む
        public static byte[] GetMemory(IntPtr process_handle, uint size, IntPtr address)
        {
            byte[] result_buffer = new byte[size];
            uint get_byte_size;
            if (!ReadProcessMemory(process_handle, address, result_buffer, size, out get_byte_size))
            {
                return null;
            }
            return result_buffer;
        }

    }

}
