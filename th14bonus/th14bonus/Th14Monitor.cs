﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

namespace th14bonus
{
    class Th14Monitor : IDisposable
    {
        private String target_process_name = "th14";
        private Process target_process;
        private Process GetTH11Process()
        {
            Process[] processes = Process.GetProcessesByName(target_process_name);
            if (processes.Length == 1)
            {
                return processes[0];
            }
            return null;
        }

        private void ActivateProcess(Process target_process)
        {
            Win32.ShowWindow(target_process.MainWindowHandle, Win32.ShoWindowFlags.SHOW);
            Win32.SetForegroundWindow(target_process.MainWindowHandle);
        }

        public bool Init()
        {
            target_process = GetTH11Process();
            if (target_process == null)
            {
                return false;
            }
            //プロセスをフォアグラウンドに
            ActivateProcess(target_process);
            return true;
        }

        private static bool IsNull(IntPtr ptr)
        {
            return ptr.Equals(IntPtr.Zero);
        }

        public bool Update(out int bonus_count)
        {
            bonus_count = -1;
            //プロセス手に入らない系
            if (target_process.HasExited)
            {
                return false;
            }
            IntPtr process_handle = Win32.OpenProcess(Win32.ProcessAccessFlags.PROCESS_VM_READ, false, target_process.Id);
            if (IsNull(process_handle))
            {
                return false;
            }
            byte[] tmp = Win32.GetMemory(process_handle, 4, (IntPtr)0x4F5894);
            bonus_count = BitConverter.ToInt32(tmp, 0);
            return true;
        }

        public void Dispose()
        {
            this.target_process.Dispose();
        }
    }
}
