﻿namespace th14bonus
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bonusCountTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ui_timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // bonusCountTextBox
            // 
            this.bonusCountTextBox.Location = new System.Drawing.Point(88, 12);
            this.bonusCountTextBox.Name = "bonusCountTextBox";
            this.bonusCountTextBox.Size = new System.Drawing.Size(100, 22);
            this.bonusCountTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "0.5 bonus";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ui_timer
            // 
            this.ui_timer.Tick += new System.EventHandler(this.Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 58);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bonusCountTextBox);
            this.Name = "Form1";
            this.Text = "th14bonus";
            this.Load += new System.EventHandler(this.form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox bonusCountTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer ui_timer;
    }
}

